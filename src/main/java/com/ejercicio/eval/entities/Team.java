package com.ejercicio.eval.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name="team", uniqueConstraints = {@UniqueConstraint(columnNames = {"cve_team"})})
public class Team {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer idTeam;
	
	private String name;
	
	@Column(name="cve_team")
	private String cveTeam;
	
	@Column(name = "fundation_at")
	private Integer fundationAt;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "created_at")
	private Date createdAt = new Date(System.currentTimeMillis());
	
	@Column(name = "modified_at")
	@Temporal(TemporalType.TIMESTAMP)
	private Date modifiedAt = new Date(System.currentTimeMillis());;

	@ManyToOne
	@JoinColumn(name = "id_country")
	private Country country;
	
	
	public Team() {
	}

	public Team(String name, String cveTeam, Integer fundationAt) {
		this.name = name;
		this.cveTeam = cveTeam;
		this.fundationAt = fundationAt;		
	}

	public Integer getIdTeam() {
		return idTeam;
	}

	public void setIdTeam(Integer idTeam) {
		this.idTeam = idTeam;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCveTeam() {
		return cveTeam;
	}

	public void setCveTeam(String cveTeam) {
		this.cveTeam = cveTeam;
	}

	public Integer getFundationAt() {
		return fundationAt;
	}

	public void setFundationAt(Integer fundationAt) {
		this.fundationAt = fundationAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getModifiedAt() {
		return modifiedAt;
	}

	public void setModifiedAt(Date modifiedAt) {
		this.modifiedAt = modifiedAt;
	}

	public Country getCountry() {
		return country;
	}

	public void setCountry(Country country) {
		this.country = country;
	}
	
	
	

}
