package com.ejercicio.eval.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "country")
@JsonIgnoreProperties(value = {"teams"})
public class Country {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Integer idCountry;
	
	private String name;
	
	@Column(name = "country_code")
	private String countryCode;
	
	@OneToMany(mappedBy = "country", cascade = CascadeType.PERSIST)
	private List<Team> teams;

	
	public Country() {
	}

	public Country(String name, String countryCode) {
		this.name = name;
		this.countryCode = countryCode;
	}

	public Integer getIdCountry() {
		return idCountry;
	}

	public void setIdCountry(Integer idCountry) {
		this.idCountry = idCountry;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCountryCode() {
		return countryCode;
	}

	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}
	
	
}
