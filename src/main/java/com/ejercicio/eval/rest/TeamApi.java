package com.ejercicio.eval.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.ejercicio.eval.dto.TeamDTO;
import com.ejercicio.eval.services.impl.TeamService;

@RestController
@RequestMapping("api/team")
public class TeamApi {
	
	@Autowired
	private TeamService teamService;

	@GetMapping("/all")
	public ResponseEntity<?> findAllTeams(){
		return this.teamService.getAllTeams();
	}
	
	@GetMapping
	public ResponseEntity<?> findByTeamName(@RequestParam(name = "name") String name){
		return this.teamService.getTeamByName(name);
	}
	
	@PostMapping
	public ResponseEntity<?> saveTeam(@RequestBody TeamDTO teamDTO){
		return this.teamService.saveTeam(teamDTO);
	}
	
	@PutMapping
	public ResponseEntity<?> updateTeam(@RequestBody TeamDTO teamDTO){
		return this.teamService.updateTeam(teamDTO);
	}
	
}
