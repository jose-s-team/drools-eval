package com.ejercicio.eval.dto;

public class TeamDTO {
	
	private Integer idTeam;
	private String name;
	private String cveTeam;
	private Integer fundationYear;
	private Integer idCountry;
	
	
	
	public TeamDTO(String name, String cveTeam, Integer fundationYear, Integer idCountry) {
		this.name = name;
		this.cveTeam = cveTeam;
		this.fundationYear = fundationYear;
		this.idCountry = idCountry;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCveTeam() {
		return cveTeam;
	}
	public void setCveTeam(String cveTeam) {
		this.cveTeam = cveTeam;
	}
	public Integer getFundationYear() {
		return fundationYear;
	}
	public void setFundationYear(Integer fundationYear) {
		this.fundationYear = fundationYear;
	}
	public Integer getIdCountry() {
		return idCountry;
	}
	public void setIdCountry(Integer idCountry) {
		this.idCountry = idCountry;
	}
	public Integer getIdTeam() {
		return idTeam;
	}
	public void setIdTeam(Integer idTeam) {
		this.idTeam = idTeam;
	}
	
	
	

}
