package com.ejercicio.eval.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ejercicio.eval.entities.Country;

@Repository
public interface CountryRepository extends JpaRepository<Country, Integer> {

}
