package com.ejercicio.eval.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.ejercicio.eval.entities.Team;

@Repository
public interface TeamRepository extends JpaRepository<Team, Integer> {

	Optional<Team> findByName(String name);

	Optional<Team> findByCveTeam(String name);

}
