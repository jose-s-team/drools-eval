package com.ejercicio.eval.util;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.ejercicio.eval.entities.Country;
import com.ejercicio.eval.entities.Team;
import com.ejercicio.eval.repositories.CountryRepository;
import com.ejercicio.eval.services.CountryService;
import com.ejercicio.eval.services.impl.TeamService;

@Component
public class DataInitialzr implements CommandLineRunner{

	@Autowired
	private CountryService countryService;
	
	@Autowired
	private TeamService teamService;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@Override
	public void run(String... args) throws Exception {
		Country country = new Country("México", "MX");
		Country country2 = new Country("Argentina", "AR");
		Country country3 = new Country("Australia", "AU");
		Country country4 = new Country("Chile", "CL");
		Country country5 = new Country("Colmbia", "CO");
		Country country6 = new Country("España", "ES");
		this.countryService.saveList(Arrays.asList(country,country2, country3, country4, country5, country6));
		
		Team team = new Team("FC Barcelona", "FCBARCA", 1980);
		team.setCountry(this.countryRepository.findById(6).get());
		
		Team team2 = new Team("Santos", "Santos", 1981);
		team2.setCountry(this.countryRepository.findById(1).get());
		
		Team team3 = new Team("Boca Juniors", "FCBOCAJR", 1982);
		team3.setCountry(this.countryRepository.findById(2).get());	
		
		this.teamService.saveList(Arrays.asList(team, team2, team3));
		
		
		
		
	}

}
