package com.ejercicio.eval;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DroolsExamApplication {

	public static void main(String[] args) {
		SpringApplication.run(DroolsExamApplication.class, args);
	}

}
