package com.ejercicio.eval.services.impl;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.ejercicio.eval.dto.TeamDTO;
import com.ejercicio.eval.entities.Country;
import com.ejercicio.eval.entities.Team;
import com.ejercicio.eval.repositories.TeamRepository;
import com.ejercicio.eval.services.CountryService;
import com.ejercicio.eval.services.TeamServiceRepository;

@Service
public class TeamService implements TeamServiceRepository {

	@Autowired
	private TeamRepository teamRepository;
	
	@Autowired
	private CountryService countryService;
	
	@Override
	public ResponseEntity<?> getAllTeams() {
		
		return ResponseEntity.ok(this.teamRepository.findAll());
	}

	@Override
	public ResponseEntity<?> updateTeam(TeamDTO teamDTO) {
		Optional<Team> teamOp = this.teamRepository.findById(teamDTO.getIdTeam());
		
		Team team = teamOp.get();
		
		if(Objects.nonNull(teamDTO.getIdCountry()) || teamDTO.getIdCountry() != 0) {
			Optional<Country> countryOp = this.countryService.findById(teamDTO.getIdCountry());
			if(countryOp.isEmpty())
				return ResponseEntity.status(HttpStatus.NOT_FOUND).body("El país no ha sido encontrado");
			
			team.setCountry(countryOp.get());
		}
		
		team.setCveTeam(teamDTO.getCveTeam());
		team.setName(teamDTO.getName());
		team.setModifiedAt(new Date(System.currentTimeMillis()));
		
		if(Objects.nonNull(teamDTO.getFundationYear()) || teamDTO.getFundationYear() != 0)
			team.setFundationAt(teamDTO.getFundationYear());
		this.teamRepository.save(team);
		return ResponseEntity.status(HttpStatus.OK).body(team);
	}

	@Override
	public ResponseEntity<?> saveTeam(TeamDTO teamDTO) {
		// TODO Auto-generated method stub
		
		Optional<Country> countryOp = this.countryService.findById(teamDTO.getIdCountry());
		if(!countryOp.isPresent())
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("El país no ha sido encontrado");
		
		Optional<Team> teamOptional = this.teamRepository.findByName(teamDTO.getName());
		if(teamOptional.isPresent())
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("El nombre de team ya existe");
	
		Optional<Team> teamCveOptional = this.teamRepository.findByCveTeam(teamDTO.getCveTeam());
		
		if(teamCveOptional.isPresent())
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("La clave del team ya existe");
		
		Team team = new Team(teamDTO.getName(), teamDTO.getCveTeam(), teamDTO.getFundationYear());
		team.setCountry(countryOp.get());
		
		this.teamRepository.save(team);
		
		return ResponseEntity.status(HttpStatus.CREATED).body(team);
	}

	@Override
	public void deleteTem(Team team) {
		// TODO Auto-generated method stub
		
	}

	public void saveList(List<Team> asList) {
		// TODO Auto-generated method stub
		this.teamRepository.saveAll(asList);
	}

	@Override
	public ResponseEntity<?> getTeamByName(String name) {
		Optional<Team> teamOptinal = this.teamRepository.findByName(name);
		
		if(teamOptinal.isPresent())
			return ResponseEntity.status(HttpStatus.OK).body(teamOptinal.get());
		else
			return ResponseEntity.status(HttpStatus.NOT_FOUND).body("El team no existe");
	}


}
