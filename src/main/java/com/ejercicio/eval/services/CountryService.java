package com.ejercicio.eval.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.ejercicio.eval.entities.Country;
import com.ejercicio.eval.repositories.CountryRepository;

@Service
public class CountryService {

	@Autowired
	private CountryRepository countryRepository;
	
	public void saveList(List<Country> countries) {
		this.countryRepository.saveAll(countries);
	}

	public Optional<Country> findById(Integer idCountry) {
		// TODO Auto-generated method stub
		return this.countryRepository.findById(idCountry);
	}
}
