package com.ejercicio.eval.services;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;

import com.ejercicio.eval.dto.TeamDTO;
import com.ejercicio.eval.entities.Team;

public interface TeamServiceRepository {

	ResponseEntity<?> getAllTeams();
	
	ResponseEntity<?> updateTeam(TeamDTO team);
	
	ResponseEntity<?> saveTeam(TeamDTO team);
	
	ResponseEntity<?> getTeamByName(String name);
	
	void deleteTem(Team team);
	
	
}
